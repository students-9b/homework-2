import java.util.Scanner;

public class Number_12 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter town: ");
        String town = scan.nextLine();
        System.out.println("Enter sales: ");
        double sales = Double.parseDouble(scan.nextLine());

        if (town.equals("Sofia") && sales >= 0 && sales <= 500) {
            System.out.println(sales * 0.05);
        } else if (town.equals("Sofia") && sales > 500 && sales <= 1000) {
            System.out.println(sales * 0.07);
        } else if (town.equals("Sofia") && sales >= 10000 && sales < 1000) {
            System.out.println(sales * 0.08);
        } else if (town.equals("Sofia") && sales > 10000) {
            System.out.println(sales * 0.12);
        } else if (town.equals("Varna") && sales >= 0 && sales <= 500) {
            System.out.println(sales * 0.045);
        } else if (town.equals("Varna") && sales > 500 && sales <= 1000) {
            System.out.println(sales * 0.075);
        } else if (town.equals("Varna") && sales >= 10000 && sales < 1000) {
            System.out.println(sales * 0.1);
        } else if (town.equals("Varna") && sales > 10000) {
            System.out.println(sales * 0.13);
        } else if (town.equals("Plovdiv") && sales >= 0 && sales <= 500) {
            System.out.println(sales * 0.055);
        } else if (town.equals("Plovdiv") && sales > 500 && sales <= 1000) {
            System.out.println(sales * 0.08);
        } else if (town.equals("Plovdiv") && sales >= 10000 && sales < 1000) {
            System.out.println(sales * 0.12);
        } else if (town.equals("Plovdiv") && sales > 10000) {
            System.out.println(sales * 0.145);
        } else {
            System.out.println("error");
        }
    }

}

