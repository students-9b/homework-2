import java.util.Scanner;

public class Number_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number between -100 and 100: ");
        int number = scanner.nextInt();

        if (number >= -100 && number <= 100 && (number != 0)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");

        }
    }
}