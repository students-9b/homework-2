import java.util.Scanner;

public class Number_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter time of the day: ");
        int time = scanner.nextInt();
        System.out.println("Enter day of the week: ");
        String day = scanner.nextLine();

        if (time >= 10 && time <= 18) {
            System.out.println("The office is opened.");
        } else {
            System.out.println("The office is closed.");
        }

        switch (day) {
            case "Monday":
            case "Tuesday":
            case "Wednesday":
            case "Thursday":
            case "Friday":
            case "Saturday":
                break;
            case "Sunday":
                System.out.println("In Sunday the office is closed");
                return;
            default:
                System.out.println("Error");
                break;

        }

    }

}

