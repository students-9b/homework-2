import java.util.Scanner;

public class Number_5 {
    public static void main (String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter product (coffee, water, beer, sweets, peanuts): ");
        String product = scanner.nextLine();
        System.out.println("Enter town (Sofia, Plovdiv, Varna): ");
        String town = scanner.nextLine();
        System.out.println("Enter quantity: ");
        double quantity = scanner.nextDouble();
        double price = 0;

        switch (town) {
            case "Sofia":
                switch (product) {
                    case "coffee":
                        price = 0.50;
                        break;
                    case "water":
                        price = 0.80;
                        break;
                    case "beer":
                        price = 1.20;
                        break;
                    case "sweets":
                        price = 1.45;
                        break;
                    case "peanuts":
                        price = 1.60;
                        break;
                    default:
                        System.out.println("Невалиден продукт!");
                        return;
                }
                break;
            case "Plovdiv":
                switch (product) {
                    case "coffee":
                        price = 0.40;
                        break;
                    case "water":
                        price = 0.70;
                        break;
                    case "beer":
                        price = 1.15;
                        break;
                    case "sweets":
                        price = 1.30;
                        break;
                    case "peanuts":
                        price = 1.50;
                        break;
                    default:
                        System.out.println("Невалиден продукт!");
                        return;
                }
                break;
            case "Varna":
                switch (product) {
                    case "coffee":
                        price = 0.45;
                        break;
                    case "water":
                        price = 0.70;
                        break;
                    case "beer":
                        price = 1.10;
                        break;
                    case "sweets":
                        price = 1.35;
                        break;
                    case "peanuts":
                        price = 0.55;
                        break;
                    default:
                        System.out.println("Невалиден продукт! ");
                        return;
                }
                break;
            default:
                System.out.println("Невалиден град! ");
                break;
        }

        double totalPrice = price * quantity;
        System.out.println("Общата цена е " + totalPrice + "лв.");
    }
}

